import {ADD_PROC, REMOVE_PROC, APPEAR_PROC} from '../actions/procs';

const initialState = {
    'csgo.exe': {
        name: 'csgo.exe',
        isActive: false,
    },
    node: {
        name: 'node',
        isActive: false,
    },
    '/bin/bash': {
        name: '/bin/bash',
        isActive: false,
    },
    '/opt/Telegram/Telegram': {
        name: '/opt/Telegram/Telegram',
        isActive: false,
    },
};

export default function procs(state=initialState, action) {
    let procName = action.payload && action.payload.procName;

    if (action.type === ADD_PROC)
        return {...state, [procName]: {name: procName, isActive: false}};

    if (action.type === REMOVE_PROC) {
        let {[procName]: deleted, ...newState} = state;
        return newState;
    }

    if (action.type === APPEAR_PROC) {
        // procName: ['bash', 'node', ...] - unique proc names
        let procs = {};
        for (let [key, proc] of Object.entries(state)) {
            if (procName.includes(key)) {
                if (!proc.isActive)
                    procs[key] = {...proc, isActive: true};
            } else {
                if (proc.isActive)
                    procs[key] = {...proc, isActive: false};
            }
        }
        if (Object.keys(procs).length === 0)
            return state;
        return {...state, ...procs};
    }

    return state;
}
