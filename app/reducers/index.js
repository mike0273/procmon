// @flow
import { combineReducers } from 'redux';
import { routerReducer as router } from 'react-router-redux';
import procs from './procs';

const rootReducer = combineReducers({
  procs,
  router,
});

export default rootReducer;
