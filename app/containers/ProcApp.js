import { connect } from 'react-redux';
import { addProc, removeProc, appearProc } from '../actions/procs';
import ProcApp from '../components/ProcApp';

function mapStateToProps(state) {
    return {
        procs: state.procs,
    };
}

function mapDispatchToProps(dispatch) {
    return {
        addProc: procName => dispatch(addProc(procName)),
        removeProc: procName => dispatch(removeProc(procName)),
        appearProc: procName => dispatch(appearProc(procName)),
    };
}

const ProcAppContainer = connect(mapStateToProps, mapDispatchToProps)(ProcApp);

export default ProcAppContainer;
