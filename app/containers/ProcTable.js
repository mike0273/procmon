import { connect } from 'react-redux';
import { removeProc } from '../actions/procs';
import ProcTable from '../components/ProcTable';

function mapStateToProps(state) {
    return {
        procs: state.procs,
    };
}

function mapDispatchToProps(dispatch) {
    return {
        removeProc: procName => dispatch(removeProc(procName)),
    };
}

const ProcTableContainer = connect(mapStateToProps, mapDispatchToProps)(ProcTable);

export default ProcTableContainer;
