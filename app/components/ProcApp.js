import PropTypes from 'prop-types';
import React, { Component } from 'react';
import Typography from 'material-ui/Typography';
import Card, { CardActions, CardContent } from 'material-ui/Card';
import { PROCESS_CHECK_INTERVAL } from '../constants';
import { psUniqueFilter } from '../utils/ps';
import ProcTable from '../containers/ProcTable';
import AddProc from './AddProc';

export default class ProcApp extends Component {
    static propTypes = {
        procs: PropTypes.object.isRequired,
        addProc: PropTypes.func.isRequired,
        removeProc: PropTypes.func.isRequired,
        appearProc: PropTypes.func.isRequired,
    };

    getWatchedProcNames() {
        return Object.keys(this.props.procs);
    }

    checkProcs() {
        psUniqueFilter(this.getWatchedProcNames(), 'name').
            then(procs => {
                this.props.appearProc(procs.map(proc => proc.name));
            });
    }

    componentDidMount() {
        this.timerId = setInterval(() => this.checkProcs(), PROCESS_CHECK_INTERVAL);
    }

    componentWillUnmount() {
        clearInterval(this.timerId);
    }

    render() {
        return (
            <Card>
                <Typography color="primary" variant="headline" paragraph={true}>
                    Process Monitor
                </Typography>
                <CardActions>
                    <AddProc addProc={this.props.addProc} />
                </CardActions>
                <CardContent>
                    <ProcTable />
                </CardContent>
            </Card>
        );
    }
}
