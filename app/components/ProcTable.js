import PropTypes from 'prop-types';
import React, { Component } from 'react';
import Button from 'material-ui/Button';
import DeleteIcon from '@material-ui/icons/Delete';
import Table, { TableBody, TableCell, TableHead, TableRow } from 'material-ui/Table';
import Paper from 'material-ui/Paper';
import styles from './ProcTable.css';

export default function ProcTable({procs, removeProc}) {
  return (
    <Paper>
      <Table>
        <TableHead>
          <TableRow>
            <TableCell>Process Name</TableCell>
            <TableCell>State</TableCell>
            <TableCell> </TableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          {Object.values(procs).map(row => {
            return (
              <TableRow key={row.name}>
                <TableCell><strong>{row.name}</strong></TableCell>
                <TableCell><div className={row.isActive ? styles['led-green'] : styles['led-grey']}></div></TableCell>
                <TableCell>
                    <Button variant="fab" onClick={() => removeProc(row.name)}>
                        <DeleteIcon />
                    </Button>
                </TableCell>
              </TableRow>
            );
          })}
        </TableBody>
      </Table>
    </Paper>
  );
}

ProcTable.propTypes = {
  procs: PropTypes.object.isRequired,
  removeProc: PropTypes.func.isRequired,
};
