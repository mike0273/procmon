import PropTypes from 'prop-types';
import React, { Component } from 'react';
import TextField from 'material-ui/TextField';
import Button from 'material-ui/Button';
import AddIcon from '@material-ui/icons/Add';


export default class AddProc extends Component {
    static propTypes = {
        addProc: PropTypes.func.isRequired,
    };

    state = {
        procName: '',
    };

    addProc() {
        let procName = this.state.procName.trim()
        this.setState({procName: ''});
        if (!procName)
            return;
        this.props.addProc(procName);
    }

    render() {
        return (
            <div>
                <Button variant="fab" color="primary" onClick={() => this.addProc()} style={{float: 'right'}}>
                    <AddIcon />
                </Button>
                <TextField
                    id="procName"
                    label="Process Name"
                    helperText="Please enter a process name"
                    onChange={ev => this.setState({procName: ev.target.value})}
                    value={this.state.procName}
                    autoFocus={true}
                    fullWidth={false}
                    margin="normal"
                />
            </div>
        );
    }
}
