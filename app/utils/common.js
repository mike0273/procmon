export function isPrimitive(v) {
	return v !== Object(v);
}

export function uniqueObjList(list, key) {
    const res = [];
    const keysSet = new Set();
    for (let el of list) {
        if (!el.hasOwnProperty(key))
            continue;
        if (keysSet.has(el[key]))
            continue;
        keysSet.add(el[key]);
        res.push(el);
    }
    return res;
}
