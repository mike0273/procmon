import psList from 'ps-node';
import { uniqueObjList } from './common';


export function ps() {
    return new Promise((resolve, reject) => psList.lookup(
                {command: ''},
                (err, procs) => {
                    if (err) {
                        //reject([]);
                        resolve([]);
                    } else {
                        resolve(procs.map(proc => ({pid: proc.pid, name: proc.command})));
                    }
                })
            );
}


export function psFilter(procNames=[]) {
    return ps().
        then(procs => procs.filter(proc => procNames.includes(proc.name)));
}

export function psUniqueFilter(procNames=[], key) {
    return psFilter(procNames).then(procs => uniqueObjList(procs, key));
}
