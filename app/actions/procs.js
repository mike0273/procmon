export const ADD_PROC = 'ADD_PROC';
export const REMOVE_PROC = 'REMOVE_PROC';
export const APPEAR_PROC = 'APPEAR_PROC';

export function addProc(procName) {
    return {
        type: ADD_PROC,
        payload: {
            procName,
        },
    };
}

export function removeProc(procName) {
    return {
        type: REMOVE_PROC,
        payload: {
            procName,
        },
    };
}

export function appearProc(procNames=[]) {
    return {
        type: APPEAR_PROC,
        payload: {
            // procName: ['bash', 'node', ...] - unique proc names
            procName: procNames,
        },
    };
}
